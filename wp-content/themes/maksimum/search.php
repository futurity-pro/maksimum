<?php
get_header();
$wp_query = new WP_Query();
$search = $_GET['s'];
$options = get_option('maksimum_options');
if(strpos($search, '_') && strpos($search, '-')){
    $what = array('-','_');
    $search =  str_replace($what,'',$search);
}
$x_array = array();
$x_array[] = array(
        'key'     => 'phone',
        'value'   => $search,
        'compare' => 'LIKE'
);
    

 
if (!empty($x_array)) {
        //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
	$wp_query->query(array(
                'post_type'      => 'girls',
		'meta_query'     => $x_array,
                //'posts_per_page' => $options['image_number'],
                'post_status'    => 'publish',
                //'page'           => $paged
	));
}
//var_dump($x_array);
?>
<?php
$commenter = wp_get_current_commenter();
$fields =  array(
    'author' => '<div class="row"><input id="name" type="text" name="author" placeholder="Имя*" class="required" /></div>',
    'captcha' => '<div class="captcha-holder">						
            <script type="text/javascript"
               src="http://www.google.com/recaptcha/api/challenge?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm">
            </script>
            <noscript>
               <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm"
                   height="300" width="500" frameborder="0"></iframe><br>
               <textarea name="recaptcha_challenge_field" rows="3" cols="40">
               </textarea>
               <input type="hidden" name="recaptcha_response_field"
                   value="manual_challenge">
            </noscript>
                    </div>',
    'rating' => '<input name="rating" value="0" type="hidden" />'
);
 
$comments_args = array(
    'comment_field' => '<div class="row"><textarea id="comments" cols="30" rows="5" name="comment" placeholder="Комментарий*" class="required"></textarea></div>',
    'fields' =>  $fields,
    'title_reply'=>'',
    'id_form' => 'form-reviews',
    'label_submit' => 'Отправить',
    'comment_notes_after' => '',
    'comment_notes_before' => ''
    
);
 

?>
<div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <div id="form-for-reviews-horder">
            <div class="what-to-do-holder">
                <div class="title">
                    <h2>Оставить отзыв</h2>
                    <a href="#" class="close"></a>
                </div>
            <div class="rating-holder">
                <div class="what-to-do" style="display: none"></div>
                <span>Ваша оценка *</span>
                <div class="rating comment">
                    <ul>
                        <li><a id="one" title="Плохо" href="javascript"></a></li>
                        <li><a id="two" title="Приемлемо" href="#"></a></li>
                        <li><a id="three" title="Средне" href="#"></a></li>
                        <li><a id="four" title="Хорошо" href="#"></a></li>
                        <li><a id="five" title="Отлично" href="#"></a></li>
                    </ul>
                </div>
            </div>
            <?php comment_form($comments_args,1)?>
            </div>
        </div>
        <div id="mask"></div>
    </div>
</div>
<div id="page_main">
    <?php if ( $wp_query->have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
    <?php
    $args = array(
	'status' => 'approve',
	'post_id' => $post->ID
        );
        $comments = get_comments($args);
        $count = 0;
        $all_ranks = 0;
        foreach($comments as $comment) :
                $all_ranks += get_comment_meta( $comment->comment_ID, 'rating', true );
                $count++;
        endforeach;
        if ($count>0):
            $rank = $all_ranks/$count;
        else:
            $rank = 0;
        endif;
?>
        <?php if ( $images = get_posts( array(
                'post_parent' => $post->ID,
                'post_type' => 'attachment',
                'numberposts' => -1,
                'orderby' => 'title',
                'order' => 'ASC',
                'post_mime_type' => 'image',
                'exclude' => $thumb_ID,
            )))
                {
                        $small_image_url = wp_get_attachment_image($images[0]->ID, 'archive-profile');
                }?>
            <div class="model">
                <div class="model-info">
                    <div class="title">
                        <a href="<?php the_permalink()?>" target="_blank"><h3><?php echo the_title();?></h3></a>
                        <div class="rating" data-rating="<?php echo round($rank);?>">
                            <ul>
                                <li><a title="очень плохо" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="плохо" name="modal" rel="<?php echo $post->ID;?>" href="#form-for-reviews-horder"></a></li>
                                <li><a title="средне" name="modal" rel="<?php echo $post->ID;?>" href="#form-for-reviews-horder"></a></li>
                                <li><a title="хорошо" name="modal" rel="<?php echo $post->ID;?>" href="#form-for-reviews-horder"></a></li>
                                <li><a title="очень хорошо" name="modal" rel="<?php echo $post->ID;?>" href="#form-for-reviews-horder"></a></li>
                            </ul>
 			</div>
                    </div>
                    <p><?php echo $post->post_excerpt?></p>
                    <span>Тел. <?php echo get_post_meta($post->ID, "phone", true)?></span>
                    <a href="<?php the_permalink()?>" target="_blank">Читать далее</a>
                </div>
                <div class="model-photo-holder">
                    <div class="model-photo">
                        <a class="model-photo" href="<?php the_permalink()?>" target="_blank">
                            <?php if (isset($images[0])){ echo $small_image_url;}?>
                        </a>
                        <!--img class="biggg" src="<?php echo $large_image_url[0]?>" alt="<?php echo the_title_attribute('echo=0')?>"-->
                    </div>
                </div>
            </div>
        <?php endwhile;?>
        <?php //maksimum_pagination();?>
    <?php else : ?>
        <?php echo 'Анкет с таким номером телефона не найдено.'?>
    <?php endif; ?>
</div>
<?php get_footer(); ?>