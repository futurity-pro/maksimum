<?php
$commenter = wp_get_current_commenter();
$fields =  array(
    'author' => '<div class="row"><input id="name" type="text" name="author" placeholder="Имя*" class="required" /></div>',
    'captcha' => '<div class="captcha-holder">						
            <script type="text/javascript"
               src="https://www.google.com/recaptcha/api/challenge?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm">
            </script>
            <noscript>
               <iframe src="https://www.google.com/recaptcha/api/noscript?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm"
                   height="300" width="500" frameborder="0"></iframe><br>
               <textarea name="recaptcha_challenge_field" rows="3" cols="40">
               </textarea>
               <input type="hidden" name="recaptcha_response_field"
                   value="manual_challenge">
            </noscript>
                    </div>',
    'rating' => '<input name="rating" value="" class="required" type="hidden" />'
);
 
$comments_args = array(
    'comment_field' => '<div class="row"><textarea id="comments" cols="30" rows="5" name="comment" placeholder="Комментарий*" class="required"></textarea></div>',
    'fields' =>  $fields,
    'title_reply'=>'',
    'id_form' => 'form-reviews',
    'label_submit' => 'Отправить',
    'comment_notes_after' => '',
    'comment_notes_before' => ''
    
);
 

?>

<div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <div id="form-for-reviews-horder">
            <div class="what-to-do-holder">
                <div class="title">
                    <h2>Оставить отзыв</h2>
                    <a href="#" class="close"></a>
                </div>
			<?php if(!current_user_can('level_10')){?>
				<div class="rating-holder">
					<div class="what-to-do" style="display: none"></div>
					<span>Ваша оценка *</span>
					<div class="rating comment">
						<ul>
							<li><a id="one" title="Плохо" href="javascript"></a></li>
							<li><a id="two" title="Приемлемо" href="#"></a></li>
							<li><a id="three" title="Средне" href="#"></a></li>
							<li><a id="four" title="Хорошо" href="#"></a></li>
							<li><a id="five" title="Отлично" href="#"></a></li>
						</ul>
					</div>
				</div>
			<?php } else {?>
				<div class="adm">Администратор не может голосовать или оставлять отзывы.</div>
			<?php } ?>
            <?php comment_form($comments_args)?>
            </div>
        </div>
        <div id="mask"></div>
    </div>
</div>

