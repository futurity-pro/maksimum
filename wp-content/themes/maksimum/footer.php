﻿</div>
<?php
$post = $wp_query->get_queried_object();
//		$postType = $post->post_type;
?>
<?php $options = get_option('maksimum_options'); ?>
<?php $excludeArray = array('faq', 'submit', 'rules'); ?>

<?php /* if( !in_array($post->post_name, $excludeArray)
			AND $options['banner_left']!= '' || $options['banner_right']!= ''){?>
				<?php if($options['banner_left']!= ''){?>
						<?php echo $options['banner_left']?>
				<?php } ?>
				<?php if($options['banner_right']!= ''){?>
						<?php echo $options['banner_right']?>
				<?php } ?>                 
		<?php } */
?>
</div>
<!-- footer -->
<div id="footer">
    <div class="footer-holder">
        <div style="position:absolute;margin-left:165px;margin-top: -5px;">
            <!--LiveInternet counter-->
            <script type="text/javascript"><!--
                document.write("<a href='http://www.liveinternet.ru/click' " +
                    "target=_blank><img src='//counter.yadro.ru/hit?t28.1;r" +
                    escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                    ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                        screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                    ";" + Math.random() +
                    "' alt='' title='LiveInternet: показано количество просмотров и" +
                    " посетителей' " +
                    "border='0' width='88' height='120'><\/a>")
                //--></script>
            <!--/LiveInternet-->
        </div>
        <ul class="footer-nav">
            <div class="footer-nav-part">
                <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'footer-left')); ?>
            </div>
            <div class="footer-nav-part">
                <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'footer-right')); ?>
            </div>
        </ul>
        <div class="footer-bottom">
            <span class="copyrigth">&copy; intimbaza.nl</span>
        </div>
    </div>

</div>
<?php wp_footer(); ?>
<!-- Google Analytics counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45596647-2', 'intimbaza.nl');
  ga('send', 'pageview');

</script>
<!-- /Google Analytics counter -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter25262594 = new Ya.Metrika({id:25262594,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/25262594" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>
