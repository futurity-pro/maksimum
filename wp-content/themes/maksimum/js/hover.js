jQuery(document).ready(function(){
  function lockScroll(){
    $html = $('html'); 
    $body = $('body'); 
    var initWidth = $body.outerWidth();
    var initHeight = $body.outerHeight();

    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    $html.data('scroll-position', scrollPosition);
    $html.data('previous-overflow', $html.css('overflow'));
    $html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);   

    var marginR = $body.outerWidth()-initWidth;
    var marginB = $body.outerHeight()-initHeight; 
    $body.css({'margin-right': marginR,'margin-bottom': marginB});
     $('body').css('margin-right', marginR);
 };
 function unlockScroll(){
    $html = $('html');
    $body = $('body');
    $html.css('overflow', $html.data('previous-overflow'));
    var scrollPosition = $html.data('scroll-position');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);    

    $body.css('margin-right', 0);
    $body.css('margin-bottom', 0);
 };
 jQuery('a[name=modal]').click(function(e) {
  e.preventDefault();
  lockScroll();
  var id = jQuery(this).attr('href');
  var comment_id = jQuery(this).attr('rel');
  jQuery('#comment_post_ID').val(comment_id);
  //clearing comments forms
    jQuery('#recaptcha_reload').click();
    jQuery('#name').val('');
    jQuery('#comments').val('');
    jQuery('.what-to-do').css('display','none');
    jQuery('input[name=rating]').val("");
    jQuery('.comment a').removeClass("active");
    jQuery('.ajax-load').css('display','none');
  
  
  //alert(comment_id);
  
  jQuery(id).css('top', '-1000px');
  var maskHeight = jQuery(document).height();
  var maskWidth = jQuery(window).width();
  jQuery('#mask').css({'width':maskWidth,'height':maskHeight});
  jQuery('#mask').fadeIn(100);
  var winH = jQuery(window).height();
  var winW = jQuery(window).width();
  jQuery(id).css('display', 'block');
  jQuery(id).animate({"top":"100px"}, 300);
  jQuery(id).css('left', winW/2-jQuery(id).width()/2);
 });
 jQuery('#form-for-reviews-horder .close').click(function (e) {
  e.preventDefault();
  unlockScroll();
  jQuery('#form-for-reviews-horder').css('top', '-1000px');
  jQuery('#mask, #form-for-reviews-horder').hide();
 });
 jQuery('#mask').click(function () {
  jQuery(this).hide();
  unlockScroll();
  jQuery('#form-for-reviews-horder').css('top', '-1000px');
  jQuery('#form-for-reviews-horder').hide();
 });
});


jQuery(document).ready(function(){
    function lockScroll(){
    $html = $('html'); 
    $body = $('body'); 
    var initWidth = $body.outerWidth();
    var initHeight = $body.outerHeight();

    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    $html.data('scroll-position', scrollPosition);
    $html.data('previous-overflow', $html.css('overflow'));
    $html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);   

    var marginR = $body.outerWidth()-initWidth;
    var marginB = $body.outerHeight()-initHeight; 
    $body.css({'margin-right': marginR,'margin-bottom': marginB});
     $('body').css('margin-right', marginR);
 };
 function unlockScroll(){
    $html = $('html');
    $body = $('body');
    $html.css('overflow', $html.data('previous-overflow'));
    var scrollPosition = $html.data('scroll-position');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);    

    $body.css('margin-right', 0);
    $body.css('margin-bottom', 0);
 };
 jQuery('a[name=modaler]').click(function(e) {
  e.preventDefault();
  var id = jQuery(this).attr('href');
  lockScroll();
  jQuery(id).css('top', '-1000px');
  var maskHeight = jQuery(document).height();
  var maskWidth = jQuery(window).width();
  jQuery('#masking').css({'width':maskWidth,'height':maskHeight});
  jQuery('#masking').fadeIn(100);
  var winH = jQuery(window).height();
  var winW = jQuery(window).width();
  jQuery(id).css('display', 'block');
  jQuery(id).animate({"top":"100px"}, 300);
  jQuery(id).css('left', winW/2-jQuery(id).width()/2);
 });
 jQuery('#form-for-reviews-horder-second .close').click(function (e) {
  e.preventDefault();
  unlockScroll();
  jQuery('#form-for-reviews-horder-second').css('top', '-1000px');
  jQuery('#masking, #form-for-reviews-horder-second').hide();
 });
 jQuery('#masking').click(function () {
  jQuery(this).hide();
  unlockScroll();
  jQuery('#form-for-reviews-horder-second').css('top', '-1000px');
  jQuery('#form-for-reviews-horder-second').hide();
 });
});

jQuery(document).ready(function(){
    function lockScroll(){
    $html = $('html'); 
    $body = $('body'); 
    var initWidth = $body.outerWidth();
    var initHeight = $body.outerHeight();

    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    $html.data('scroll-position', scrollPosition);
    $html.data('previous-overflow', $html.css('overflow'));
    $html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);   

    var marginR = $body.outerWidth()-initWidth;
    var marginB = $body.outerHeight()-initHeight; 
    $body.css({'margin-right': marginR,'margin-bottom': marginB});
     $('body').css('margin-right', marginR);
 };
 function unlockScroll(){
    $html = $('html');
    $body = $('body');
    $html.css('overflow', $html.data('previous-overflow'));
    var scrollPosition = $html.data('scroll-position');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);    

    $body.css('margin-right', 0);
    $body.css('margin-bottom', 0);
 };
 jQuery('a[name=modaled]').click(function(e) {
  e.preventDefault();
  lockScroll();
  var id = jQuery(this).attr('href');
  jQuery(id).css('top', '-1000px');
  var maskHeight = jQuery(document).height();
  var maskWidth = jQuery(window).width();
  jQuery('#masker').css({'width':maskWidth,'height':maskHeight});
  jQuery('#masker').fadeIn(100);
  var winH = jQuery(window).height();
  var winW = jQuery(window).width();
  jQuery(id).css('display', 'block');
  jQuery(id).animate({"top":"100px"}, 300);
  jQuery(id).css('left', winW/2-jQuery(id).width()/2);
 });
 jQuery('#form-for-reviews-horderer-third .close').click(function (e) {
  e.preventDefault();
  unlockScroll();
  jQuery('#form-for-reviews-horderer-third').css('top', '-1000px');
  jQuery('#masker, #form-for-reviews-horderer-third').hide();
 });
 jQuery('#masker').click(function () {
  jQuery(this).hide();
  unlockScroll();
  jQuery('#form-for-reviews-horderer-third').css('top', '-1000px');
  jQuery('#form-for-reviews-horderer-third').hide();
 });
});