// AJAXified commenting system
jQuery('document').ready(function($){
    var commentform=$('.comment-form'); // find the comment form
        commentform.append('<div id="comment-status"></div>'); // add info panel before the form to provide feedback or errors
    $("#form-reviews").validate({
            submitHandler: function(form) {
                var statusdiv=$('#comment-status'); // define the infopanel
                //commentform.submit(function(){
                //serialize and store form data in a variable
                var formdata=commentform.serialize();
                //Add a status message
                statusdiv.html('<span class="ajax-load"></span>');
                //Extract action URL from commentform
                var formurl=commentform.attr('action');
                //Post Form with data
                $.ajax({
                        type: 'post',
                        url: formurl,
                        data: formdata,
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                        //statusdiv.html('<p class="wdpajax-error" >You might have left one of the fields blank, or be posting too quickly</p>');
                        //alert('error');
                    },
                    success: function(data, textStatus){
                        if(data=="success")
                            //statusdiv.html('<p class="ajax-success" >Thanks for your comment. We appreciate your response.</p>');
                            //alert('success');
                           function unlockScroll(){
                                $html = $('html');
                                $body = $('body');
                                $html.css('overflow', $html.data('previous-overflow'));
                                var scrollPosition = $html.data('scroll-position');
                                window.scrollTo(scrollPosition[0], scrollPosition[1]);    

                                $body.css('margin-right', 0);
                                $body.css('margin-bottom', 0);
                             };
                            $('#mask, #form-for-reviews-horder').hide();
                            commentform.find('input[name=author]').val('');
                            commentform.find('textarea[name=comment]').val('');
                            //commentform.find('input[name=rating]').val('');
                            commentform.find('input[name=recaptcha_response_field]').val('');
                            
                            var maskHeight = $(document).height();
                            var maskWidth = $(window).width();
                            $('#masked').css({'width':maskWidth,'height':maskHeight});
                            $('#masked').fadeIn(100);
                            var winH = $(window).height();
                            var winW = $(window).width();
                            $('#form-for-reviews-horderer').css('display', 'block');
                            $('#form-for-reviews-horderer').animate({"top":"100px"}, 300);
                            $('#form-for-reviews-horderer').css('left', winW/2-$('#form-for-reviews-horderer').width()/2);
                            
                            $('#form-for-reviews-horderer .close').click(function (e) {
								e.preventDefault();
                                $('#form-for-reviews-horderer').css('top', '-1000px');
                                $('#masked, #form-for-reviews-horderer').hide();
                                unlockScroll();
                            });
                           $('#masked').click(function () {
                                $(this).hide();
                                $('#form-for-reviews-horderer').css('top', '-1000px');
                                $('#form-for-reviews-horderer').hide();
                                unlockScroll();
                           });
 
                    }
                });
                return false;
               // });
            },
            onfocusout: false,
            onkeyup:false,
            ignore: "input[type='text']:hidden",
            errorLabelContainer:'.what-to-do',
            rules: {
                author:"required",
                comment:"required",
                rating:"required",
                recaptcha_response_field: {
                    required: true,
                    remote: { 
                        url:my_data.template_directory_uri+"/captcha/verifyCaptcha.php",
                        type:"post",
                        async:true,
                        data: {
                            recaptcha_challenge_field: function(){ return $('#recaptcha_challenge_field').val(); },
                            recaptcha_response_field: function(){ return $('#recaptcha_response_field').val(); }
                        }
                    }
                }
            },
            messages: {
                author:"Введите имя. ",
                comment:"Введите текст комментария. ",
                rating:"Введите оценку. ",
                recaptcha_response_field: {
                    required: "Введите код с картинки. ",
                    remote: "Введите правильный код с картинки. "
                }
            }
            });
    
});