/*
 * jQuery File Upload Plugin Angular JS Example 1.2.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, regexp: true */
/*global window, angular */

(function () {
    'use strict';

    var url = my_data.template_directory_uri+'/fileuploader/uploader.php';

    angular.module('demo', [
        'blueimp.fileupload'
    ])
        .config([
            '$httpProvider', 'fileUploadProvider',
            function ($httpProvider, fileUploadProvider) {
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                fileUploadProvider.defaults.redirect = window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                );
                //if (isOnGitHub) {
                    // Demo settings:
                    angular.extend(fileUploadProvider.defaults, {
                        // Enable image resizing, except for Android and Opera,
                        // which actually support image resizing, but fail to
                        // send Blob objects via XHR requests:
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        maxFileSize: 2000000,
                        maxNumberOfFiles: 10,
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|jpg|png)$/i
                    });
                //}
            }
        ])

        .controller('DemoFileUploadController', [
            '$scope', '$http', '$filter', '$window',
            function ($scope, $http) {
            //validations
            $scope.options = {
                url: url
            };
            /*$scope.deleteItem = function(one) {
                if (one) {
                  var _id = $scope.selected[0];
                  Items['delete']({id:$scope.items[_id].id}, function() {
                    $scope.items.splice(_id,1);
                    $scope.selected = [];
                  });
                } else {
                    var ids = [];
                    angular.forEach($scope.selected, function(_id) { ids.push($scope.items[_id].id); });
                    Items['delete']({ids:ids}, function(){
                      angular.forEach($scope.selected, function(_id) { $scope.items.splice(_id,1); });
                      $scope.selected = [];
                    });
                  }
              };*/
            $scope.$on('fileuploadadd', function (e, data) {
                    
                    var photos_quantity = $('.added-photo').children().length;
                    //var file = $scope.file;
                    //console.log(photos_quantity);
                    
                    if (photos_quantity > 9) {
                       //$scope.clear(file);
                        /*for( var i = photos_quantity; i >  9; i-- ){
                            var photo =  $('.added-photo').children().eq(i);
                            photo.children('.cancel').click();
                        }*/

                    }
  
                    
                });
            $scope.$on('fileuploadsubmit', function (e, data) {
                /*var imagesNumber = $scope.queue.length - $scope.processing();
                if(imagesNumber==0){
                        $('.what-to-do').append('<label for="images" class="error">Загрузите фото. </label>');
                        $('.what-to-do').css('display','block');
                        e.preventDefault();
                    }*/
                if($('input[name=postTitle]').val() == '' ||$('input[name=postPhone]').val() == '' ||$('input[name=postContent]').val() == '' ||
                        $('input[name=recaptcha_response_field]').val() == ''||$('input[name=postEmail]').val() == ''){
                        e.preventDefault();
                }
            });
            }
        ])

        .controller('FileDestroyController', [
            '$scope', '$http',
            function ($scope, $http) {
                var file = $scope.file,
                    state;
                if (file.url) {
                    file.$state = function () {
                        return state;
                    };
                    file.$destroy = function () {
                        state = 'pending';
                        return $http({
                            url: file.deleteUrl,
                            method: file.deleteType
                        }).then(
                            function () {
                                state = 'resolved';
                                $scope.clear(file);
                            },
                            function () {
                                state = 'rejected';
                            }
                        );
                    };
                } else if (!file.$cancel && !file._index) {
                    file.$cancel = function () {
                        $scope.clear(file);
                    };
                }
            }
        ]);
       

}());
