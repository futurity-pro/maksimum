﻿<?php
/**
Template Name: Вопросы и ответы
 */

get_header(); ?>
<?php
	$faqs = new WP_Query('post_type=faq&post_status=publish&orderby=date&order=asc');
 ?>
<div id="faq">
	<?php if($faqs->have_posts())
	{
		while($faqs->have_posts())
		{
		$faqs->the_post();?>
		<div class="answer-the-question">
			<h3><?php echo the_title();?></h3>
			<?php echo the_content();?>
		</div>
	<?php }
	}?>
</div>
<?php get_footer(); ?>