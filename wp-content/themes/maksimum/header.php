<?php

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html style="overflow-y:scroll" class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html style="overflow-y:scroll" class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html style="overflow-y:scroll" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php
    
    wp_title();

    ?></title> 
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico"/>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/js/jquery.bind-first-0.1.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hover.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ajaxcomments.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jcarousellite_1.0.1.js"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <?php $options = get_option('maksimum_options');
        if ($options['mature'] == 1) {
            ?>
            <div id="form-for-reviews-horder-second">
                <div class="attention-windows">
                    <div class="title">
                        <h2>Внимание</h2>
                        <a href="#close" class="close"></a>
                    </div>
                    <div class="attention-text">
                        <?php
                        $p = get_page(177);
                        echo apply_filters('the_content', $p->post_content);
                        ?>
                    </div>
                    <a href="#close" class="close">Закрыть</a>
                </div>
            </div>
            <div id="masking"></div>
        <?php } ?>
    </div>
</div>
<div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <div id="form-for-reviews-horderer">
            <div class="attention-windows">
                <div class="title">
                    <h2>Внимание</h2>
                    <a href="#close" class="close"></a>
                </div>
                <div class="attention-text">
                    <?php
                    $p = get_page(239);
                    echo apply_filters('the_content', $p->post_content);
                    ?>
                </div>
                <a href="#close" class="close">Закрыть</a>
            </div>
        </div>
        <div id="masked"></div>
    </div>
</div>
<!--div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <div id="form-for-reviews-horderer-third">
            <div class="attention-windows">
                <div class="title">
                    <h2>Введите номер вашей анкеты</h2>
                    <a href="#close" class="close"></a>
                </div>	
                <div class="attention-text">
                    <div class="errored" style="display: none"></div>
                    <form id="profile-form" action="" method="post">
                        <input id="profile" name="profile" class="required" type="text" value="" placeholder="Номер анкеты*" />
                    </form>
                </div>
                <a id="submit" href="">Войти</a>	
            </div>
        </div>
        <div id="masker"></div>
    </div>
</div-->
<div id="wrapper">

    <?php $post = $wp_query->get_queried_object(); ?>
    <?php if ($post->post_name != 'submit') { ?>
        <a class="up" href="#"><img src="<?php echo get_template_directory_uri() ?>/images/icon-up.png" alt=""></a>
    <?php } ?>

    <div id="header">
        <div class="header-holder">
            <div class="logo-holder">
                <strong class="logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><img
                            src="<?php echo get_template_directory_uri() ?>/images/intimbaza-logo.png"
                            alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"></a>
                    <a name="modaler" class="age-limit" href="#form-for-reviews-horder-second"></a>
                </strong>
                <span><?php bloginfo('description'); ?></span>
                <?php $options = get_option('maksimum_options'); ?>
                <strong class="slogan"><?php echo $options['header_text'] ?></strong>
            </div>
            <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'header', 'container_class' => 'nav-holder', 'menu_id' => 'nav')); ?>
            <!--div class="enter-button">
                <a name="modaled" href="#form-for-reviews-horderer-third">Вход</a>
            </div-->
        </div>
    </div>

    <div id="main">