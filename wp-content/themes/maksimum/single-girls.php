<?php
get_header(); ?>
<?php 
 if ( has_post_thumbnail()) {
   $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
 }
 

 
 $args = array(
	'status' => 'approve',
	'post_id' => $post->ID
);
$comments = get_comments($args);
$count = 0;
$all_ranks = 0;
foreach($comments as $comment) :
	$all_ranks += get_comment_meta( $comment->comment_ID, 'rating', true );
        $count++;
endforeach;
if ($count>0):
    $rank = $all_ranks/$count;
else:
    $rank = 0;
endif;
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<div id="profile">
    <div class="model-in-detail">
            <div class="model-info-holder">
                    <div class="model-info-d">
                            <div class="title">
                                    <h3><?php echo the_title() ?></h3>
                                    <div class="rating" data-rating="<?php echo round($rank);?>">
                                        <ul>
                                            <li><a title="Плохо" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                            <li><a title="Приемлемо" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                            <li><a title="Средне" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                            <li><a title="Хорошо" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                            <li><a title="Отлично" rel="<?php echo $post->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <p><?php echo $post->post_content ?></p>
                            <span>Тел. <?php echo get_post_meta($post->ID, "phone", true)?></span>
                    </div>
                <?php if (true)
                                {
                                    $images = get_posts( array(
                                        'post_parent' => $post->ID,
                                        'post_type' => 'attachment',
                                        'numberposts' => -1,
                                        'orderby' => 'title',
                                        'order' => 'ASC',
                                        'post_mime_type' => 'image',
                                        'exclude' => $thumb_ID,
                                    ))

                                    ?>
								<?php if(count($images)>=5){?>
								<script>
									$(function(){
										$(".slider-mini .slider-mini-wrapper").jCarouselLite({
											btnPrev: ".slider-button-prev",
											btnNext: ".slider-button-next",
											visible: 4,
                                            circular: false
										});
									})		
									</script>
									<?php } ?>
                                    <div class="slider-mini" <?php if (count($images)<5) echo "style=text-align:center;"?>>
                                            <a class="slider-button-prev" <?php if (count($images)<5) echo "style=display:none"?> href="javascript:"><img src="<?php echo get_template_directory_uri()?>/images/slider-prev.png" alt=""></a>
                                            <div class="slider-mini-wrapper" <?php if (count($images)>=5) echo "style='margin-left:38px !important;'"?>>
                                                <ul>
                                                    <?php
													$count = 1;
                                                    foreach( $images as $image ) {
                                                        $attachmentImage = wp_get_attachment_image_src($image->ID, 'profile-slider');
                                                        $attachmentImageHref = wp_get_attachment_image_src($image->ID, 'large');
                                                        echo '<li><a class="fancybox" rel="group1" href="'.$attachmentImageHref[0].'"><img alt="Фотография '.$count.'" src="' .$attachmentImage[0]. '"></a></li>';
														$count++;
                                                    } ?>
                                                </ul>
                                            </div>
                                            <a class="slider-button-next" <?php if (count($images)<5) echo "style=display:none"?> href="javascript:"><img src="<?php echo get_template_directory_uri()?>/images/slider-next.png" alt=""></a>
                                    </div>
                                    <?php }?>
									<?php if(get_post_meta($post->ID, "video", true) != '') {?>
									<?php $youtube_url = get_post_meta($post->ID, "video", true);
										  $youtube = str_replace("http://youtu.be/","",$youtube_url);
									?>
											<div class="video">
												<?php echo '<iframe width="640" height="360" src="https://www.youtube.com/embed/'.$youtube.'?rel=0&showinfo=0&fs=1&vq=hd720&wmode=opaque" frameborder="0" allowfullscreen></iframe>'?>
											</div>
									<?php } ?>
            </div>
    </div>
   
    <div class="reviews">
            <h4>Отзывы</h4>
            <?php comments_template(); ?>
            <?php wp_list_comments( array( 'callback' => 'maksimum_comments_callback' ) );?>
            <div class="reviews-button">
                    <a name="modal"  rel="<?php echo $post->ID;?>" href="#form-for-reviews-horder">Оставить отзыв</a>
            </div>
    </div>
<?php get_footer(); ?>