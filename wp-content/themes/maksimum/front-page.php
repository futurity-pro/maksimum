<?php get_header(); ?>
<?php

global $wpdb;
//posts randomizer
if($_GET['pn'] == 1 || !$_GET['pn']) {
    if( isset( $_SESSION['seed'] ) ) {
            unset( $_SESSION['seed'] );
    }
}
$seed = false;
if( isset( $_SESSION['seed'] ) ) {
    $seed = $_SESSION['seed'];
}
if ( ! $seed ) {
    $seed = rand();
    $_SESSION['seed'] = $seed;
}
$result = 'RAND('.$seed.')';

$count_posts = wp_count_posts('girls');
$published_posts = $count_posts->publish;
$nr = $published_posts;
if (isset($_GET['pn'])) {
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
} else {
    $pn = 1;
}
$options = get_option('maksimum_options');
$itemsPerPage = $options['image_number'];
$lastPage = ceil($nr / $itemsPerPage);
if ($pn < 1) {
    $pn = 1;
} else if ($pn > $lastPage) {
    $pn = $lastPage;
}
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage;
$girls = $wpdb->get_results( 
	"
	SELECT *
	FROM $wpdb->posts
	WHERE post_status = 'publish' 
		AND post_type = 'girls'
                ORDER BY $result $limit
	"
);
global $withcomments; $withcomments = true;
        comments_template();
?>
<div id="page_main">
    <?php if ( $girls ) : ?>
        <?php foreach ( $girls as $girl ): ?>
    <?php
    $args = array(
	'status' => 'approve',
	'post_id' => $girl->ID
        );
        $comments = get_comments($args);
        $count = 0;
        $all_ranks = 0;
        foreach($comments as $comment) :
                $all_ranks += get_comment_meta( $comment->comment_ID, 'rating', true );
                $count++;
        endforeach;
        if ($count>0):
            $rank = $all_ranks/$count;
        else:
            $rank = 0;
        endif;
        
?>
        <?php if ( $images = get_posts( array(
                'post_parent' => $girl->ID,
                'post_type' => 'attachment',
                'numberposts' => -1,
                'orderby' => 'title',
                'order' => 'ASC',
                'post_mime_type' => 'image',
                'exclude' => $thumb_ID,
            )))
                {
                        $thumb = wp_get_attachment_image( get_post_thumbnail_id($girl->ID), 'archive-profile' );
                        if($thumb){
                            $small_image_url = $thumb;
                        } else {
                            $small_image_url = wp_get_attachment_image($images[0]->ID, 'archive-profile');
                        }
                }?>
            <div class="model">
                <div class="model-info">
                    <div class="title">
                        <a href="<?php echo get_permalink($girl->ID)?>" target="_blank"><h3><?php echo $girl->post_title;?></h3></a>
                        <div class="rating" data-rating="<?php echo round($rank);?>">
                            <ul>
                                <li><a title="Плохо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Приемлемо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Средне" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Хорошо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Отлично" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                            </ul>
 			</div>
                    </div>
                    <p><?php echo $girl->post_excerpt?></p>
                    <span>Тел. <?php echo get_post_meta($girl->ID, "phone", true)?></span>
                    <a href="<?php echo get_permalink($girl->ID)?>" target="_blank">Читать далее</a>
                </div>
                <div class="model-photo-holder">
                    <div class="model-photo">
                        <a class="model-photo" href="<?php echo get_permalink($girl->ID)?>" target="_blank">
                            <?php if (isset($images[0])){ echo $small_image_url;}?>
                        </a>
                        <!--img class="biggg" src="<?php echo $large_image_url[0]?>" alt="<?php echo the_title_attribute('echo=0')?>"-->
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php echo getPaginationString($pn, $nr, $itemsPerPage, $adjacents = 1, $targetpage = "/", $pagestring = "?pn=");?>
    <?php else : ?>
        <?php echo 'Анкет не найдено.'?>
    <?php endif; ?>
</div>
<div id="seo-footer">
<?php while ( have_posts() ) : the_post(); ?>
    <?php echo the_content();?>
<?php endwhile;?>
</div>
<?php get_footer(); ?>